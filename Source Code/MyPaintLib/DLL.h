﻿#pragma once

#include <objidl.h>
#include <gdiplus.h>
#pragma comment (lib,"Gdiplus.lib")
using namespace Gdiplus;

#ifdef PAINTLIBRARY_EXPORTS
#define PAINTLIBRARY_API __declspec(dllexport) 
#else
#define PAINTLIBRARY_API __declspec(dllimport) 
#endif

#define SHAPE_MODEL_LINE 0
#define SHAPE_MODEL_RECTANGLE 1
#define SHAPE_MODEL_ELLIPSE 2
#define SHAPE_MODEL_SQUARE 3
#define SHAPE_MODEL_CIRCLE 4

namespace PaintLibrary
{
	class CShape
	{
	protected:
		POINT lefttop;
		POINT rightbottom;
		Color color;
		DashStyle dStyle;
	public:
		PAINTLIBRARY_API CShape();
		PAINTLIBRARY_API ~CShape();

		PAINTLIBRARY_API virtual void Draw(HDC hdc, Graphics* g, Pen* p) = 0;
		PAINTLIBRARY_API virtual CShape* Create() = 0;
		PAINTLIBRARY_API virtual void setCoor(POINT lefttop, POINT rightbottom) = 0;
		PAINTLIBRARY_API virtual int getType() = 0;
		PAINTLIBRARY_API ARGB getColor();
		PAINTLIBRARY_API void setColor(ARGB value);
		PAINTLIBRARY_API DashStyle getStyle();
		PAINTLIBRARY_API void setStyle(DashStyle dStyle);
	};

	class CLine : public CShape
	{
	public:
		PAINTLIBRARY_API CLine();
		PAINTLIBRARY_API ~CLine();

		PAINTLIBRARY_API void Draw(HDC hdc, Graphics* g, Pen* p);
		PAINTLIBRARY_API CShape* Create();
		PAINTLIBRARY_API void setCoor(POINT lefttop, POINT rightbottom);
		PAINTLIBRARY_API virtual int getType();
	};

	class CRectangle : public CShape
	{
	public:
		PAINTLIBRARY_API CRectangle();
		PAINTLIBRARY_API ~CRectangle();

		PAINTLIBRARY_API void Draw(HDC hdc, Graphics* g, Pen* p);
		PAINTLIBRARY_API CShape* Create();
		PAINTLIBRARY_API void setCoor(POINT lefttop, POINT rightbottom);
		PAINTLIBRARY_API virtual int getType();
	};

	class CEllipse : public CShape
	{
	public:
		PAINTLIBRARY_API CEllipse();
		PAINTLIBRARY_API ~CEllipse();

		PAINTLIBRARY_API void Draw(HDC hdc, Graphics* g, Pen* p);
		PAINTLIBRARY_API CShape* Create();
		PAINTLIBRARY_API void setCoor(POINT lefttop, POINT rightbottom);
		PAINTLIBRARY_API virtual int getType();
	};

	class CSquare : public CShape
	{
	public:
		PAINTLIBRARY_API CSquare();
		PAINTLIBRARY_API ~CSquare();

		PAINTLIBRARY_API void Draw(HDC hdc, Graphics* g, Pen* p);
		PAINTLIBRARY_API CShape* Create();
		PAINTLIBRARY_API void setCoor(POINT lefttop, POINT rightbottom);
		PAINTLIBRARY_API virtual int getType();
	};

	class CCircle :
		public CShape
	{
	public:
		PAINTLIBRARY_API CCircle();
		PAINTLIBRARY_API ~CCircle();

		PAINTLIBRARY_API void Draw(HDC hdc, Graphics* g, Pen* p);
		PAINTLIBRARY_API CShape* Create();
		PAINTLIBRARY_API void setCoor(POINT lefttop, POINT rightbottom);
		PAINTLIBRARY_API virtual int getType();
	};
}