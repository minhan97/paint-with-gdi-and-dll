#include "stdafx.h"
#include "DLL.h"

namespace PaintLibrary
{
	CShape::CShape()
	{
	}

	CShape::~CShape()
	{
	}

	DashStyle CShape::getStyle()
	{
		return dStyle;
	}
	void CShape::setStyle(DashStyle dStyle)
	{
		this->dStyle = dStyle;
	}

	ARGB CShape::getColor()
	{
		return color.GetValue();
	}

	void CShape::setColor(ARGB value)
	{
		color.SetValue(value);
	}
	////////////////
	CLine::CLine()
	{
	}

	CLine::~CLine()
	{
	}

	void CLine::Draw(HDC hdc, Graphics* g, Pen* p)
	{
		g->DrawLine(p, lefttop.x, lefttop.y, rightbottom.x, rightbottom.y);
	}

	CShape* CLine::Create() {
		return new CLine;
	}

	void CLine::setCoor(POINT lefttop, POINT rightbottom)
	{
		this->lefttop.x = lefttop.x;
		this->lefttop.y = lefttop.y;
		this->rightbottom.x = rightbottom.x;
		this->rightbottom.y = rightbottom.y;
	}
	
	int CLine::getType()
	{
		return SHAPE_MODEL_LINE;
	}
	////////////////////////////
	CRectangle::CRectangle()
	{
	}

	CRectangle::~CRectangle()
	{
	}

	void CRectangle::Draw(HDC hdc, Graphics* g, Pen* p)
	{
		g->DrawRectangle(p, lefttop.x, lefttop.y, rightbottom.x - lefttop.x, rightbottom.y - lefttop.y);
		//Rectangle(hdc, lefttop.x, lefttop.y, rightbottom.x, rightbottom.y);
	}

	CShape* CRectangle::Create() {
		return new CRectangle();
	}

	void CRectangle::setCoor(POINT lefttop, POINT rightbottom)
	{
		this->lefttop.x = lefttop.x;
		this->lefttop.y = lefttop.y;
		this->rightbottom.x = rightbottom.x;
		this->rightbottom.y = rightbottom.y;
	}

	int CRectangle::getType()
	{
		return SHAPE_MODEL_RECTANGLE;
	}
	///////////////////////////////////////////////
	CEllipse::CEllipse()
	{
	}

	CEllipse::~CEllipse()
	{
	}

	void CEllipse::Draw(HDC hdc, Graphics* g, Pen* p)
	{
		g->DrawEllipse(p, lefttop.x, lefttop.y, rightbottom.x - lefttop.x, rightbottom.y - lefttop.y);
		//Ellipse(hdc, lefttop.x, lefttop.y, rightbottom.x, rightbottom.y);
	}

	CShape* CEllipse::Create() {
		return new CEllipse();
	}

	void CEllipse::setCoor(POINT lefttop, POINT rightbottom)
	{
		this->lefttop.x = lefttop.x;
		this->lefttop.y = lefttop.y;
		this->rightbottom.x = rightbottom.x;
		this->rightbottom.y = rightbottom.y;
	}

	int CEllipse::getType()
	{
		return SHAPE_MODEL_ELLIPSE;
	}
	/////////////////////////////////////////
	CSquare::CSquare()
	{
	}

	CSquare::~CSquare()
	{
	}

	void CSquare::Draw(HDC hdc, Graphics* g, Pen* p)
	{
		RECT a;
		a.left = min(lefttop.x, lefttop.y);
		a.top = min(rightbottom.x, rightbottom.y);
		a.right = max(lefttop.x, lefttop.y);
		a.bottom = max(rightbottom.x, rightbottom.y);

		int length = min(abs(lefttop.y - lefttop.x), abs(rightbottom.y - rightbottom.x));

		if (lefttop.x > lefttop.y)
		{
			a.left = a.right - length;
		}
		else
			a.right = a.left + length;

		if (rightbottom.y < rightbottom.x)
		{
			a.top = a.bottom - length;
		}
		else
		{
			a.bottom = a.top + length;
		}

		g->DrawRectangle(p, a.left, a.top, a.right - a.left, a.bottom - a.top);
		//Rectangle(hdc, a.left, a.top, a.right, a.bottom);

	}

	CShape* CSquare::Create() {
		return new CSquare();
	}

	void CSquare::setCoor(POINT lefttop, POINT rightbottom)
	{
		this->lefttop.x = lefttop.x;
		this->lefttop.y = lefttop.y;
		this->rightbottom.x = rightbottom.x;
		this->rightbottom.y = rightbottom.y;
	}

	int CSquare::getType()
	{
		return SHAPE_MODEL_SQUARE;
	}
	///////////////////////////////////////////
	CCircle::CCircle()
	{
	}

	CCircle::~CCircle()
	{
	}

	void CCircle::Draw(HDC hdc, Graphics* g, Pen* p)
	{
		RECT a;
		a.left = min(lefttop.x, lefttop.y);
		a.top = min(rightbottom.x, rightbottom.y);
		a.right = max(lefttop.x, lefttop.y);
		a.bottom = max(rightbottom.x, rightbottom.y);

		int length = min(abs(lefttop.y - lefttop.x), abs(rightbottom.y - rightbottom.x));

		if (lefttop.x > lefttop.y)
		{
			a.left = a.right - length;
		}
		else
			a.right = a.left + length;

		if (rightbottom.y < rightbottom.x)
		{
			a.top = a.bottom - length;
		}
		else
		{
			a.bottom = a.top + length;
		}
		g->DrawEllipse(p, a.left, a.top, a.right - a.left, a.bottom - a.top);
		//Ellipse(hdc, a.left, a.top, a.right, a.bottom);

	}

	CShape* CCircle::Create() {
		return new CCircle();
	}

	void CCircle::setCoor(POINT lefttop, POINT rightbottom)
	{
		this->lefttop.x = lefttop.x;
		this->lefttop.y = lefttop.y;
		this->rightbottom.x = rightbottom.x;
		this->rightbottom.y = rightbottom.y;
	}

	int CCircle::getType()
	{
		return SHAPE_MODEL_CIRCLE;
	}
}