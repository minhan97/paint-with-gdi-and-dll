//#pragma once
//#include "Shape.h"
//
//class CEllipse : public CShape {
//private:
//public:
//	void Draw(HDC hdc);
//	CShape* Create();
//	void SetData(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect, COLORREF color, int penStyle);
//	ShapeMode getType();
//};

#pragma once
#include "Shape.h"

class CEllipse :
	public CShape
{
public:
	CEllipse();
	~CEllipse();

	void Draw(HDC hdc, Graphics* g, Pen* p);
	CShape* Create();
	void setCoor(POINT lefttop, POINT rightbottom);
	int getType();
};
