#pragma once
#include "Shape.h"
class CCircle :
	public CShape
{
public:
	CCircle();
	~CCircle();

	void Draw(HDC hdc, Graphics* g, Pen* p);
	CShape* Create();
	void setCoor(POINT lefttop, POINT rightbottom);
	int getType();
};

