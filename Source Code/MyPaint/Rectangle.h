#pragma once
#include "Shape.h"

class CRectangle :
	public CShape
{
public:
	CRectangle();
	~CRectangle();

	void Draw(HDC hdc, Graphics* g, Pen* p);
	CShape* Create();
	void setCoor(POINT lefttop, POINT rightbottom);
	int getType();
};