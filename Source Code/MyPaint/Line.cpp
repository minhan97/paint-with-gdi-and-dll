#include "stdafx.h"
#include "Line.h"


CLine::CLine()
{
	type = SHAPE_MODEL_LINE;
}


CLine::~CLine()
{
}


void CLine::Draw(HDC hdc, Graphics* g, Pen* p)
{
	g->DrawLine(p, lefttop.x, lefttop.y, rightbottom.x, rightbottom.y);
}

CShape* CLine::Create() {
	return new CLine;
}

void CLine::setCoor(POINT lefttop, POINT rightbottom)
{
	this->lefttop.x = lefttop.x;
	this->lefttop.y = lefttop.y;
	this->rightbottom.x = rightbottom.x;
	this->rightbottom.y = rightbottom.y;
}

int CLine::getType()
{
	return type;
}