﻿#pragma once
#include "stdafx.h"
#include <objidl.h>
#include <gdiplus.h>
#pragma comment (lib,"Gdiplus.lib")
using namespace Gdiplus;

#define SHAPE_MODEL_LINE		0
#define SHAPE_MODEL_RECTANGLE	1
#define SHAPE_MODEL_ELLIPSE		2
#define SHAPE_MODEL_SQUARE		3
#define SHAPE_MODEL_CIRCLE		4

class CShape
{
protected:
	POINT lefttop;
	POINT rightbottom;
	int type;
	Color color;
	DashStyle dStyle;
public:
	CShape();
	~CShape();

	virtual void Draw(HDC hdc, Graphics* g, Pen* p) = 0;
	virtual CShape* Create() = 0;
	virtual void setCoor(POINT lefttop, POINT rightbottom) = 0;
	virtual int getType() = 0;
	DashStyle getStyle();
	void setStyle(DashStyle dStyle);
	ARGB getColor();
	void setColor(ARGB value);
};