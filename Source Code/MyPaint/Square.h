#pragma once
#include "Shape.h"
class CSquare : public CShape
{
public:
	CSquare();
	~CSquare();

	void Draw(HDC hdc, Graphics* g, Pen* p);
	CShape* Create();
	void setCoor(POINT lefttop, POINT rightbottom);
	int getType();
};