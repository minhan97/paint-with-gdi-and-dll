#include "stdafx.h"
#include "Rectangle.h"


CRectangle::CRectangle()
{
	type = SHAPE_MODEL_RECTANGLE;
}

CRectangle::~CRectangle()
{
}

void CRectangle::Draw(HDC hdc, Graphics* g, Pen* p)
{
	g->DrawRectangle(p, lefttop.x, lefttop.y, rightbottom.x - lefttop.x, rightbottom.y - lefttop.y);
	//Rectangle(hdc, lefttop.x, lefttop.y, rightbottom.x, rightbottom.y);
}

CShape* CRectangle::Create() {
	return new CRectangle();
}

void CRectangle::setCoor(POINT lefttop, POINT rightbottom)
{
	this->lefttop.x = lefttop.x;
	this->lefttop.y = lefttop.y;
	this->rightbottom.x = rightbottom.x;
	this->rightbottom.y = rightbottom.y;
}

int CRectangle::getType()
{
	return type;
}