#pragma once
#include "Shape.h"

class CLine :
	public CShape
{
public:
	CLine();
	~CLine();

	void Draw(HDC hdc, Graphics* g, Pen* p);
	CShape* Create();
	void setCoor(POINT lefttop, POINT rightbottom);
	int getType();
};