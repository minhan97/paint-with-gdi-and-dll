#include "stdafx.h"
#include "Ellipse.h"


//CShape* CEllipse::Create()
//{
//	return new CEllipse;
//}
//
//void CEllipse::SetData(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect, COLORREF color, int penStyle)
//{
//	x1 = nLeftRect;
//	y1 = nTopRect;
//	x2 = nRightRect;
//	y2 = nBottomRect;
//
//	this->type = DRAWELLIPSE;
//	this->color = color;
//	this->penStyle = penStyle;
//}
//
//void CEllipse::Draw(HDC hdc)
//{
//	if (x1 < x2)
//	{
//		int tmp = x1;
//		x1 = x2;
//		x2 = tmp;
//	}
//	if (y1 < y2)
//	{
//		int tmp = y1;
//		y1 = y2;
//		y2 = tmp;
//	}
//
//	Ellipse(hdc, x1, y1, x2, y2);
//}
//
//ShapeMode CEllipse::getType()
//{
//	return this->type;
//}

CEllipse::CEllipse()
{
	type = SHAPE_MODEL_ELLIPSE;
}


CEllipse::~CEllipse()
{
}

void CEllipse::Draw(HDC hdc, Graphics* g, Pen* p)
{
	g->DrawEllipse(p, lefttop.x, lefttop.y, rightbottom.x - lefttop.x, rightbottom.y - lefttop.y);
	//Ellipse(hdc, lefttop.x, lefttop.y, rightbottom.x, rightbottom.y);
}

CShape* CEllipse::Create() {
	return new CEllipse();
}

void CEllipse::setCoor(POINT lefttop, POINT rightbottom)
{
	this->lefttop.x = lefttop.x;
	this->lefttop.y = lefttop.y;
	this->rightbottom.x = rightbottom.x;
	this->rightbottom.y = rightbottom.y;
}

int CEllipse::getType()
{
	return type;
}