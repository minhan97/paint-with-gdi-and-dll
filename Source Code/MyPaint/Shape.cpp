#include "stdafx.h"
#include "Shape.h"


CShape::CShape()
{
}


CShape::~CShape()
{
}

DashStyle CShape::getStyle()
{
	return dStyle;
}
void CShape::setStyle(DashStyle dStyle)
{
	this->dStyle = dStyle;
}

ARGB CShape::getColor()
{
	return color.GetValue();
}

void CShape::setColor(ARGB value)
{
	color.SetValue(value);
}