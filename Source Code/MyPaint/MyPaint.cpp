﻿// MyPaint.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "MyPaint.h"

using namespace std;

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

HWND g_hWnd;
HWND hStatusBar;

bool isDrawing = FALSE;

Color curColor(255, 0, 0, 0);
DashStyle dStyle = DashStyleSolid;
POINT current;
POINT last;
vector<PaintLibrary::CShape*> g_ShapeModel;
vector<PaintLibrary::CShape*> g_Shape;

HDC          hdcMem;
HBITMAP      hbmMem;
HANDLE       hOld;

int type = SHAPE_MODEL_LINE;

GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR           gdiplusToken;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_MYPAINT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MYPAINT));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= 0;// CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYPAINT));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_MYPAINT);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{ 
   hInst = hInstance; // Store instance handle in our global variable

   g_hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW | WS_SIZEBOX,
	   CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!g_hWnd)
   {
      return FALSE;
   }

   ShowWindow(g_hWnd, nCmdShow);
   UpdateWindow(g_hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HMENU hMenu = GetMenu(hWnd);
	UINT state;
	HDC hdc;
	int x, y;
	RECT main;
	int tempType = 0;
	PaintLibrary::CShape* shape;
	switch (message)
	{
	case WM_CREATE:
	{
		HFONT hFont = CreateFont(16, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, VIETNAMESE_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Calibri"));
		hStatusBar = CreateWindowEx(0, STATUSCLASSNAME,	L"My Paint", WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP, CW_USEDEFAULT, 0, CW_USEDEFAULT, 0,
			hWnd, (HMENU)IDC_STATUS_BAR, hInst, NULL);
		SendMessage(hStatusBar, WM_SETFONT, WPARAM(hFont), TRUE);

		g_ShapeModel.push_back(new PaintLibrary::CLine());
		g_ShapeModel.push_back(new PaintLibrary::CRectangle());
		g_ShapeModel.push_back(new PaintLibrary::CEllipse());
		g_ShapeModel.push_back(new PaintLibrary::CSquare());
		g_ShapeModel.push_back(new PaintLibrary::CCircle());

		CheckMenuItem(hMenu, ID_SHAPE_LINE, MF_CHECKED);
		GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
		break;
	}
	case WM_SIZE:
		if (wParam != SIZE_MINIMIZED)
		{
			SendMessage(hStatusBar, WM_SIZE, wParam, lParam);
		}
		break;
	case WM_LBUTTONDOWN:
		x = GET_X_LPARAM(lParam);
		y = GET_Y_LPARAM(lParam);
		if (!isDrawing)
		{
			isDrawing = TRUE;
			current.x = x;
			current.y = y;
		}
		break;
	case WM_LBUTTONUP:
	{
		POINT a;
		x = GET_X_LPARAM(lParam);
		y = GET_Y_LPARAM(lParam);
		a.x = x;
		a.y = y;
		if (type == 1 && ((GetKeyState(VK_SHIFT) & 0x8000) != 0))
		{
			tempType = 1;
			type = 3;
			shape = g_ShapeModel[type]->Create();
			type = 1;
		}
		else if (type == 2 && ((GetKeyState(VK_SHIFT) & 0x8000) != 0))
		{
			tempType = 2;
			type = 4;
			shape = g_ShapeModel[type]->Create();
			type = 2;
		}
		else
		{
			shape = g_ShapeModel[type]->Create();
		}
		shape->setColor(curColor.GetValue());
		shape->setCoor(current, a);
		shape->setStyle(dStyle);
		g_Shape.push_back(shape);
		isDrawing = FALSE;
		InvalidateRect(hWnd, NULL, FALSE);
		break;
	}
	case WM_MOUSEMOVE:
	{
		x = GET_X_LPARAM(lParam);
		y = GET_Y_LPARAM(lParam);
		WCHAR buffer[128];
		wsprintf(buffer, L"%d, %dpx", x,y);
		SetWindowText(hStatusBar, buffer);

		if (isDrawing)
		{
			last.x = x;
			last.y = y;
		}

		InvalidateRect(hWnd, NULL, FALSE);
		break;
	}		
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		switch (wmId)
		{
		case ID_PENSTYLE_DASH:
			dStyle = DashStyleDash;
			break;
		case ID_PENSTYLE_DASH_DOT:
			dStyle = DashStyleDashDot;
			break;
		case ID_PENSTYLE_DOT:
			dStyle = DashStyleDot;
			break;
		case ID_PENSTYLE_SOLID:
			dStyle = DashStyleSolid;
			break;
		case ID_COLOR_OPTIONS:
			CHOOSECOLOR cc;                 // common dialog box structure 
			static COLORREF acrCustClr[16]; // array of custom colors 
			HWND hwnd;                      // owner window
			HBRUSH hbrush;                  // brush handle
			static DWORD rgbCurrent;        // initial color selection

			// Initialize CHOOSECOLOR 
			ZeroMemory(&cc, sizeof(cc));
			cc.lStructSize = sizeof(cc);
			cc.hwndOwner = hWnd;
			cc.lpCustColors = (LPDWORD)acrCustClr;
			cc.rgbResult = rgbCurrent;
			cc.Flags = CC_FULLOPEN | CC_RGBINIT;

			if (ChooseColor(&cc) == TRUE)
			{
				hbrush = CreateSolidBrush(cc.rgbResult);
				curColor.SetFromCOLORREF(COLORREF(cc.rgbResult));
			}
			break;
		case ID_SHAPE_LINE:
			state = GetMenuState(hMenu, ID_SHAPE_RECTANGLE, MF_BYCOMMAND);
			if (state & MF_CHECKED)
			{
				CheckMenuItem(hMenu, ID_SHAPE_RECTANGLE, MF_UNCHECKED);
			}

			state = GetMenuState(hMenu, ID_SHAPE_ELLIPSE, MF_BYCOMMAND);
			if (state & MF_CHECKED)
			{
				CheckMenuItem(hMenu, ID_SHAPE_ELLIPSE, MF_UNCHECKED);
			}
			CheckMenuItem(hMenu, ID_SHAPE_LINE, MF_CHECKED);

			type = SHAPE_MODEL_LINE;
			InvalidateRect(hWnd, NULL, TRUE);
			break;
		case ID_SHAPE_RECTANGLE:
			state = GetMenuState(hMenu, ID_SHAPE_LINE, MF_BYCOMMAND);
			if (state & MF_CHECKED){
				CheckMenuItem(hMenu, ID_SHAPE_LINE, MF_UNCHECKED);
			}

			state = GetMenuState(hMenu, ID_SHAPE_ELLIPSE, MF_BYCOMMAND);
			if (state & MF_CHECKED)
			{
				CheckMenuItem(hMenu, ID_SHAPE_ELLIPSE, MF_UNCHECKED);
			}

			CheckMenuItem(hMenu, ID_SHAPE_RECTANGLE, MF_CHECKED);

			type = SHAPE_MODEL_RECTANGLE;
			InvalidateRect(hWnd, NULL, TRUE);
			break;
		case ID_SHAPE_ELLIPSE:
			state = GetMenuState(hMenu, ID_SHAPE_LINE, MF_BYCOMMAND);
			if (state & MF_CHECKED)
			{
				CheckMenuItem(hMenu, ID_SHAPE_LINE, MF_UNCHECKED);
			}

			state = GetMenuState(hMenu, ID_SHAPE_RECTANGLE, MF_BYCOMMAND);
			if (state & MF_CHECKED)
			{
				CheckMenuItem(hMenu, ID_SHAPE_RECTANGLE, MF_UNCHECKED);
			}

			CheckMenuItem(hMenu, ID_SHAPE_ELLIPSE, MF_CHECKED);

			type = SHAPE_MODEL_ELLIPSE;
			InvalidateRect(hWnd, NULL, TRUE);
			break;
		case ID_FILE_NEW:
			for (int i = g_Shape.size() - 1; i >= 0; --i)
			{
				delete g_Shape[i];
				g_Shape.pop_back();
			}
			InvalidateRect(hWnd, NULL, TRUE);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;

		GetClientRect(hWnd, &main);
		hdc = BeginPaint(hWnd, &ps);

		// Create an off-screen DC for double-buffering
		hdcMem = CreateCompatibleDC(hdc);
		hbmMem = CreateCompatibleBitmap(hdc, main.right - main.left, main.bottom - main.top);

		hOld = SelectObject(hdcMem, hbmMem);
		
		int lineWidth = 2;

		FillRect(hdcMem, &main, HBRUSH(RGB(255, 255, 255)));
		SelectObject(hdcMem, GetStockBrush(NULL_BRUSH));

		for (int i = 0; i < g_Shape.size(); i++)
		{
			Graphics* graphics = new Graphics(hdcMem);
			Pen* pen = new Pen(g_Shape[i]->getColor(), lineWidth);
			pen->SetDashStyle(g_Shape[i]->getStyle());
			g_Shape[i]->Draw(hdcMem, graphics, pen);
		}

		if (isDrawing)
		{
			if (type == 1 && ((GetKeyState(VK_SHIFT) & 0x8000) != 0))
			{
				tempType = 1;
				type = 3;
				shape = g_ShapeModel[type]->Create();
				shape->setCoor(current, last);
				shape->setColor(curColor.GetValue());
				Graphics* graphics = new Graphics(hdcMem);
				Pen* pen = new Pen(shape->getColor(), lineWidth);
				pen->SetDashStyle(shape->getStyle());
				shape->Draw(hdcMem, graphics, pen);
				type = tempType;
			}
			else if (type == 2 && ((GetKeyState(VK_SHIFT) & 0x8000) != 0))
			{
				tempType = 2;
				type = 4;
				shape = g_ShapeModel[type]->Create();
				shape->setCoor(current, last);
				shape->setColor(curColor.GetValue());
				Graphics* graphics = new Graphics(hdcMem);
				Pen* pen = new Pen(shape->getColor(), lineWidth);
				pen->SetDashStyle(shape->getStyle());
				shape->Draw(hdcMem, graphics, pen);
				type = tempType;
			}
			else
			{
				shape = g_ShapeModel[type]->Create();
				shape->setCoor(current, last);
				shape->setColor(curColor.GetValue());
				Graphics* graphics = new Graphics(hdcMem);
				Pen* pen = new Pen(shape->getColor(), lineWidth);
				pen->SetDashStyle(shape->getStyle());
				shape->Draw(hdcMem, graphics, pen);
			}
		}

		BitBlt(hdc, 0, 0, main.right - main.left, main.bottom - main.top, hdcMem, 0, 0, SRCCOPY);

		// Free-up the off-screen DC
		SelectObject(hdcMem, hOld);
		DeleteObject(hbmMem);
		DeleteDC(hdcMem);

		EndPaint(hWnd, &ps);
		break;
	}
	case WM_DESTROY:
	{
		GdiplusShutdown(gdiplusToken);
		PostQuitMessage(0);
		break;
	}
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}