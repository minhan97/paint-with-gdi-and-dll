1. Thông tin
Lê Minh Ân
1512016

2.Các chức năng làm được
- Cho phép vẽ các hình theo yêu cầu (Đường thẳng, hình chữ nhật, hình vuông, hình elip, hình tròn).
- Ấn giữ SHIFT để vẽ hình tròn và hình vuông.
- Cho phép chọn màu vẽ từ hộp thoại.
- Hỗ trợ vẽ hình với 4 kiểu nét vẽ: solid, dash, dot, dash-dot.

3. Link repo: https://minhan97@bitbucket.org/minhan97/paint-with-gdi-and-dll.git

4. Video demo: https://youtu.be/gCOxhLrjWXU